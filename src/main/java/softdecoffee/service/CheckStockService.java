/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.service;

import java.util.ArrayList;
import java.util.List;
import softdecoffee.dao.CheckStockDao;
import softdecoffee.dao.CheckStockDetailDao;
import softdecoffee.model.CheckStock;
import softdecoffee.model.CheckStockDetail;

/**
 *
 * @author Lenovo
 */
public class CheckStockService {
    private ArrayList<CheckStock> checkStock;
    private CheckStockDao checkStockDao = new CheckStockDao();

    
    public CheckStock getById(String id){
        CheckStockDao checkStockDao = new CheckStockDao();       
        return checkStockDao.get(id);
    }
    
    public List<CheckStock> getCheckStocks(){
        CheckStockDao checkStockDao = new CheckStockDao();
        return checkStockDao.getAll(" CHECK_CODE asc");
    }
    
    public ArrayList<CheckStock> getCheckStocksOrderById(){              
        return (ArrayList<CheckStock>) checkStockDao.getAll(" CHECK_CODE asc");
    }

    
    public CheckStock addNew(CheckStock editedReceipt) {
        CheckStockDao checkStockDao = new CheckStockDao();
        CheckStockDetailDao checkStockDetailDao = new CheckStockDetailDao();
        CheckStock checkStock = checkStockDao.save(editedReceipt);
        for(CheckStockDetail csd:editedReceipt.getCheckStockDetails()){
            csd.setCheckStockId(checkStock.getId());
            checkStockDetailDao.save(csd);
        }
        return checkStock;
    }

    public CheckStock update(CheckStock editedReceipt) {
        CheckStockDao checkStockDao = new CheckStockDao();
        return checkStockDao.update(editedReceipt);
    }

    public int delete(CheckStock editedReceipt) {
        CheckStockDao checkStockDao = new CheckStockDao();
        return checkStockDao.delete(editedReceipt);
    }
}
