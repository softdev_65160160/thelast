/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.test;

import softdecoffee.model.Material;
import softdecoffee.service.MaterialService;



/**
 *
 * @author Nobpharat
 */
public class MaterialTestService {
    public static void main(String[] args) {
        MaterialService mat = new MaterialService();
        for(Material material : mat.getMaterials()){
            System.out.println(material );
        }
        System.out.println(mat.getById(1));
//        Material newmaMaterial1 = new Material("Coco", "Kilogram", 10, 20, 20);
//        Material newmaMaterial2 = new Material("Milk", "Liter", 20, 20, 20);
//        Material newmaMaterial3 = new Material("Sugar", "Kilogram", 30, 20, 20);
//        Material newmaMaterial4 = new Material("Coffee Seeds", "Kilogram", 40, 20, 20);
//        mat.addNew(newmaMaterial1);
//        mat.addNew(newmaMaterial2);
//        mat.addNew(newmaMaterial3);
//        mat.addNew(newmaMaterial4);
//        for(Material material : mat.getMaterials()){
//            System.out.println(material );
//        }
        
        Material upMaterial = mat.getById(2);
        upMaterial.setMinimum(100.50);
        mat.update(upMaterial);
        System.out.println(mat.getById(2));
        mat.delete(mat.getById(4));
        for(Material material : mat.getMaterials()){
            System.out.println(material );
        }
        
    }
}
