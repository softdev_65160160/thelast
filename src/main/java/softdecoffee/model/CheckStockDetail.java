/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import softdecoffee.helper.DatabaseHelper;

/**
 *
 * @author Lenovo
 */
public class CheckStockDetail {

    private String Id;
    private String MeterialId;
    private String CheckStockId;
    private int remainingAmount;
    private int expAmount;
    private int totalLost;

    public CheckStockDetail(String Id, String MeterialId, String CheckStockId, int remainingAmount, int expAmount, int totalLost) {
        this.Id = Id;
        this.MeterialId = MeterialId;
        this.CheckStockId = CheckStockId;
        this.remainingAmount = remainingAmount;
        this.expAmount = expAmount;
        this.totalLost = totalLost;
    }

    public CheckStockDetail(int remainingAmount, int expAmount, int totalLost) {
        this.Id = "-1";
        this.MeterialId = "";
        this.CheckStockId = "";
        this.remainingAmount = remainingAmount;
        this.expAmount = expAmount;
        this.totalLost = totalLost;
    }

    public CheckStockDetail() {
        this.Id = "-1";
        this.MeterialId = "";
        this.CheckStockId = "";
        this.remainingAmount = 0;
        this.expAmount = 0;
        this.totalLost = 0;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getMeterialId() {
        return MeterialId;
    }

    public void setMeterialId(String MeterialId) {
        this.MeterialId = MeterialId;
    }

    public String getCheckStockId() {
        return CheckStockId;
    }

    public void setCheckStockId(String CheckStockId) {
        this.CheckStockId = CheckStockId;
    }

    public int getRemainingAmount() {
        return remainingAmount;
    }

    public void setRemainingAmount(int remainingAmount) {
        this.remainingAmount = remainingAmount;
    }

    public int getExpAmount() {
        return expAmount;
    }

    public void setExpAmount(int expAmount) {
        this.expAmount = expAmount;
    }

    public int getTotalLost() {
        return totalLost;
    }

    public void setTotalLost(int totalLost) {
        this.totalLost = totalLost;
    }


    @Override
    public String toString() {
        return "CheckStockDetail{" + "Id=" + Id + ", MeterialId=" + MeterialId + ", CheckStockId=" + CheckStockId + ", remainningAmount=" + remainingAmount + ", expAmount=" + expAmount + ", totalLost=" + totalLost + '}';
    }

    public static CheckStockDetail fromRS (ResultSet rs) {
        CheckStockDetail checkStockDetail = new CheckStockDetail();
        try {
            checkStockDetail.setId(rs.getString("SCD_CODE"));
            checkStockDetail.setMeterialId(rs.getString("M_CODE"));
            checkStockDetail.setCheckStockId(rs.getString("CHECk_CODE"));
            checkStockDetail.setRemainingAmount(rs.getInt("SCD_REMAINING_AMOUNT"));
            checkStockDetail.setExpAmount(rs.getInt("SCD_EXPIRED_AMOUNT"));
            checkStockDetail.setTotalLost(rs.getInt("SCD_TOTAL_LOST"));
            
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkStockDetail;
    }

}
