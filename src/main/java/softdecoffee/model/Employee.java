/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class Employee {
    private String id;
    private String FName;
    private String LName;
    private String email;
    private String tel;
    private Date inDate;
    private String address;
    private String rank;
    private String status;
    private int salary;
    private Date time_in;
    private Date time_out;
    private String Br_id;

    public Employee(String id, String FName, String LName, String email, String tel, Date inDate, String address, String rank, String status, int salary, Date time_in, Date time_out, String Br_id) {
        this.id = id;
        this.FName = FName;
        this.LName = LName;
        this.email = email;
        this.tel = tel;
        this.inDate = inDate;
        this.address = address;
        this.rank = rank;
        this.status = status;
        this.salary = salary;
        this.time_in = time_in;
        this.time_out = time_out;
        this.Br_id = Br_id;
    }
    
    public Employee(String FName, String LName, String email, String tel, Date inDate, String address, String rank, String status, int salary, Date time_in, Date time_out, String Br_id) {
        this.id = "-1";
        this.FName = FName;
        this.LName = LName;
        this.email = email;
        this.tel = tel;
        this.inDate = inDate;
        this.address = address;
        this.rank = rank;
        this.status = status;
        this.salary = salary;
        this.time_in = time_in;
        this.time_out = time_out;
        this.Br_id = Br_id;
    }
    
    public Employee() {
        this.id = "-1";
        this.FName = "";
        this.LName = "";
        this.email = "";
        this.tel = "";
        this.inDate = null;
        this.address = "";
        this.rank = "";
        this.status = "";
        this.salary = 0;
        this.time_in = null;
        this.time_out = null;
        this.Br_id = "";
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFName() {
        return FName;
    }

    public void setFName(String FName) {
        this.FName = FName;
    }

    public String getLName() {
        return LName;
    }

    public void setLName(String LName) {
        this.LName = LName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public Date getInDate() {
        return inDate;
    }

    public void setInDate(Date inDate) {
        this.inDate = inDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public Date getTime_in() {
        return time_in;
    }

    public void setTime_in(Date time_in) {
        this.time_in = time_in;
    }

    public Date getTime_out() {
        return time_out;
    }

    public void setTime_out(Date time_out) {
        this.time_out = time_out;
    }

    public String getBr_id() {
        return Br_id;
    }

    public void setBr_id(String Br_id) {
        this.Br_id = Br_id;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", FName=" + FName + ", LName=" + LName + ", email=" + email + ", tel=" + tel + ", inDate=" + inDate + ", address=" + address + ", rank=" + rank + ", status=" + status + ", salary=" + salary + ", time_in=" + time_in + ", time_out=" + time_out + ", Br_id=" + Br_id + '}';
    }

    
    
    
    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getString("EMP_CODE"));
            employee.setFName(rs.getString("EMP_FNAME"));
            employee.setLName(rs.getString("EMP_LNAME"));
            employee.setEmail(rs.getString("EMP_EMAIL"));
            employee.setTel(rs.getString("EMP_TEL"));
            employee.setInDate(rs.getDate("EMP_INDATE"));
            employee.setAddress(rs.getString("EMP_ADDRESS"));
            employee.setRank(rs.getString("EMP_RANK"));
            employee.setStatus(rs.getString("EMP_STATUS"));
            employee.setSalary(rs.getInt("EMP_SALARY"));
            employee.setSalary(rs.getInt("EMP_SALARY"));
            employee.setSalary(rs.getInt("EMP_TIME_IN"));
            employee.setSalary(rs.getInt("EMP_TIME_OUT"));
            employee.setBr_id(rs.getString("BR_CODE"));
            
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }
    
    
    
}
