/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.util.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author basba
 */
public class Promotion {

    private int id;
    private String name;
    private String start;
    private String end;
    private String detail;
    private String status;
    private Date startDate;
    private Date endDate;

    public Promotion(int id, String name, String start, String end, String detail, String status) {
        this.id = id;
        this.name = name;
        this.start = start;
        this.end = end;
        this.detail = detail;
        this.status = status;
    }

    public Promotion(String name, String start, String end, String detail, String status) {
    this.id = -1;
    this.name = name;
    this.start = start;
    this.end = end;
    this.detail = detail;
    this.status = status;
}


    public Promotion() {
        this.id = -1;
        this.name = "";
        this.start = null;
        this.end = null;
        this.detail = "";
        this.status = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String startDate) {
        this.start = startDate;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String endDate) {
        this.end = endDate;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    

    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", name=" + name + ", startDate=" + startDate + ", endDate=" + endDate + ", detail=" + detail + ", status=" + status + '}';
    }

    public static Promotion fromRS(ResultSet rs) {
        Promotion promotion = new Promotion();
        try {
            promotion.setId(rs.getInt("PRO_CODE"));
            promotion.setName(rs.getString("PRO_NAME"));
            promotion.setStartDate(rs.getTimestamp("PRO_STARTDATE"));
            promotion.setEndDate(rs.getTimestamp("PRO_ENDDATE"));
            promotion.setDetail(rs.getString("PRO_DETAIL"));
            promotion.setStatus(rs.getString("PRO_STATUS"));

        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotion;
    }
    
    
}
