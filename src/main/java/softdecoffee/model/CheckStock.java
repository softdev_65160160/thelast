/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Date;

/**
 *
 * @author Lenovo
 */
public class CheckStock {
    private String Id;
    private String employeeId;
    private Date checkDate;
    private ArrayList<CheckStockDetail> checkStockDetails = new ArrayList();

    public CheckStock(String Id, String employeeId, Date checkDate) {
        this.Id = Id;
        this.employeeId = employeeId;
        this.checkDate = checkDate;
    }
    
    public CheckStock(String employeeId,Date checkDate) {
        this.Id = "-1";
        this.employeeId = employeeId;
        this.checkDate = checkDate;
    }
    
    public CheckStock() {
        this.Id = "-1";
        this.employeeId = "";
        this.checkDate = null;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public ArrayList<CheckStockDetail> getCheckStockDetails() {
        return checkStockDetails;
    }

    public void setCheckStockDetails(ArrayList<CheckStockDetail> checkStockDetails) {
        this.checkStockDetails = checkStockDetails;
    }

    @Override
    public String toString() {
        return "CheckStock{" + "Id=" + Id + ", employeeId=" + employeeId + ", checkDate=" + checkDate + ", checkStockDetails=" + checkStockDetails + '}';
    }
    
//    public void addCheckStockDetail(CheckStock checkStock){
//        CheckStockDetail checkStockDetail = checkStock.get();
//        checkStockDetails.add(checkStock);       
//    }
    
//    public void addCheckStockDetail(CheckStock checkStock,int qty){
//        ReceiptDetail rd = new ReceiptDetail(product.getId(),product.getName(),product.getPrice(),qty,qty*product.getPrice(),-1);
//        receiptDetails.add(rd);
//        calculateTotal();
//    }
    
    public void delCheckStockDetail(CheckStockDetail checkStockDetail){
        checkStockDetails.remove(checkStockDetail);
    }
    

    public static CheckStock fromRS(ResultSet rs) {
        CheckStock checkStock = new CheckStock();
        try {
            checkStock.setId(rs.getString("CHECK_CODE"));
            checkStock.setEmployeeId(rs.getString("EMP_CODE"));
            checkStock.setCheckDate(rs.getTimestamp("CHECK_DATETIME"));        
        } catch (SQLException ex) {
            Logger.getLogger(checkStock.getId()).log(Level.SEVERE, null, ex);
            return null;
        }
        System.out.println("model"+checkStock);
        return checkStock;
    }
    
    
}
