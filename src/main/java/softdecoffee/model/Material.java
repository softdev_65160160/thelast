/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.model;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nobpharat
 */
public class Material {
    private int id;
    private String name;
    private String unit;
    private double  qoh;
    private double price ;
    private double minimum;

    public Material(int id, String name, String unit, double qoh, double price, double minimum) {
        this.id = id;
        this.name = name;
        this.unit = unit;
        this.qoh = qoh;
        this.price = price;
        this.minimum = minimum;
    }
    
    public Material(String name, String unit, double qoh, double price, double minimum) {
        this.id = -1;
        this.name = name;
        this.unit = unit;
        this.qoh = qoh;
        this.price = price;
        this.minimum = minimum;
    }
    public Material() {
        this.id = -1;
        this.name = "";
        this.unit = "";
        this.qoh = 0;
        this.price = 0;
        this.minimum = 0;
    }

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getQoh() {
        return qoh;
    }

    public void setQoh(double qoh) {
        this.qoh = qoh;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getMinimum() {
        return minimum;
    }

    public void setMinimum(double minimum) {
        this.minimum = minimum;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "Material{" + "id=" + id + ", name=" + name + ", unit=" + unit + ", qoh=" + qoh + ", price=" + price + ", minimum=" + minimum + '}';
    }
    
    public static Material fromRS(ResultSet rs) {
        Material material = new Material();
        try {
            material.setId(rs.getInt("M_CODE"));
            material.setName(rs.getString("M_NAME"));
            material.setQoh(rs.getDouble("M_QOH"));
            material.setPrice(rs.getDouble("M_PRICE_PER_UNIT"));
            material.setMinimum(rs.getDouble("M_MIN"));
            material.setUnit(rs.getString("M_UNIT"));
            
        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return material;
    }

    
}
