/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import softdecoffee.helper.DatabaseHelper;
import softdecoffee.model.CheckStock;
import softdecoffee.model.CheckStockDetail;

/**
 *
 * @author Lenovo
 */
public class CheckStockDetailDao implements Dao<CheckStockDetail>{
    @Override
    public CheckStockDetail get(String id) {
        CheckStockDetail checkStockDetail = null;
        String sql = "SELECT * FROM STOCK_CHECK_DETAIL WHERE SCD_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkStockDetail = CheckStockDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkStockDetail;
    }

    public List<CheckStockDetail> getAll() {
        ArrayList<CheckStockDetail> list = new ArrayList();
        String sql = "SELECT * FROM STOCK_CHECK_DETAIL";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockDetail checkStockDetail = CheckStockDetail.fromRS(rs);
                list.add(checkStockDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<CheckStockDetail> getAll(String where, String order) {
        ArrayList<CheckStockDetail> list = new ArrayList();
        String sql = "SELECT * FROM CHECK_STOCK where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockDetail checkStockDetail = CheckStockDetail.fromRS(rs);
                list.add(checkStockDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    

    public List<CheckStockDetail> getAll(String order) {
        ArrayList<CheckStockDetail> list = new ArrayList();
        String sql = "SELECT * FROM STOCK_CHECK_DETAIL ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStockDetail checkStockDetail = CheckStockDetail.fromRS(rs);
                list.add(checkStockDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckStockDetail save(CheckStockDetail obj) {

        String sql = "INSERT INTO STOCK_CHECK_DETAIL ( M_CODE, CHECK_CODE, SCD_REMAINING_AMOUNT , SCD_EXPIRED_AMOUNT , SCD_TOTAL_LOST)"
                + "VALUES( ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);           
            stmt.setString(1, obj.getMeterialId());
            stmt.setString(2, obj.getCheckStockId());
            stmt.setInt(3, obj.getRemainingAmount());
            stmt.setInt(4, obj.getExpAmount());
            stmt.setInt(5, obj.getTotalLost());
            
//            System.out.println(stmt);
            stmt.executeUpdate();
            String id = ""+DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckStockDetail update(CheckStockDetail obj) {
        String sql = "UPDATE CHECK_STOCK"
                + " SET M_CODE = ?, CHECK_CODE = ?, SCD_REMAINING_AMOUNT = ?, SCD_EXPIRED_AMOUNT = ?, SCD_TOTAL_LOST = ?"
                + " WHERE SCD_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getMeterialId());
            stmt.setString(2, obj.getCheckStockId());
            stmt.setInt(3, obj.getRemainingAmount());
            stmt.setInt(4, obj.getExpAmount());
            stmt.setInt(5, obj.getTotalLost());
            
            
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckStockDetail obj) {
        String sql = "DELETE FROM STOCK_CHECK_DETAIL WHERE SCD_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
}
