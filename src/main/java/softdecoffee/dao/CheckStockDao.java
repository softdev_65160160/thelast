/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdecoffee.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import softdecoffee.helper.DatabaseHelper;
import softdecoffee.model.CheckStock;

/**
 *
 * @author Lenovo
 */
public class CheckStockDao implements Dao<CheckStock>{
    @Override
    public CheckStock get(String id) {
        CheckStock checkStock = null;
        String sql = "SELECT * FROM CHECK_STOCK WHERE CHECK_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkStock = CheckStock.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkStock;
    }

    public List<CheckStock> getAll() {
        ArrayList<CheckStock> list = new ArrayList();
        String sql = "SELECT * FROM CHECK_STOCK";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStock checkStock = CheckStock.fromRS(rs);
                list.add(checkStock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    @Override
    public List<CheckStock> getAll(String where, String order) {
        ArrayList<CheckStock> list = new ArrayList();
        String sql = "SELECT * FROM CHECK_STOCK where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStock checkStock = CheckStock.fromRS(rs);
                list.add(checkStock);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<CheckStock> getAll(String order) {
        ArrayList<CheckStock> list = new ArrayList();
        String sql = "SELECT * FROM CHECK_STOCK ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckStock checkStock = CheckStock.fromRS(rs);
                list.add(checkStock);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckStock save(CheckStock obj) {

        String sql = "INSERT INTO CHECK_STOCK (ENP_CODE)"
                + "VALUES(?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getEmployeeId());        
//            System.out.println(stmt);
            stmt.executeUpdate();
            String id = ""+DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckStock update(CheckStock obj) {
        String sql = "UPDATE CHECK_STOCK"
                + " SET EMP_CODE = ?"
                + " WHERE CHECK_CODE = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getEmployeeId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(CheckStock obj) {
        String sql = "DELETE FROM MATERIAL WHERE CHECK_CODE=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }
}
