--
-- File generated with SQLiteStudio v3.4.4 on อา. ต.ค. 8 00:17:21 2023
--
-- Text encoding used: UTF-8
--
PRAGMA foreign_keys = off;
BEGIN TRANSACTION;

-- Table: BRANCH
DROP TABLE IF EXISTS BRANCH;

CREATE TABLE IF NOT EXISTS BRANCH (
    BR_CODE    INTEGER       PRIMARY KEY ASC AUTOINCREMENT
                             UNIQUE,
    BR_COMPANY VARCHAR (25)  NOT NULL,
    BR_NAME    VARCHAR (25)  NOT NULL,
    BR_ADDRESS VARCHAR (100) NOT NULL,
    BR_PHONE   CHAR (12)     CHECK (BR_PHONE LIKE '___-___-____') 
                             NOT NULL,
    EMP_CODE   INTEGER       REFERENCES EMPLOYEE (EMP_CODE) ON DELETE RESTRICT
                                                            ON UPDATE CASCADE,
    FOREIGN KEY (
        EMP_CODE
    )
    REFERENCES EMPLOYEE (EMP_CODE) 
);

INSERT INTO BRANCH (
                       BR_CODE,
                       BR_COMPANY,
                       BR_NAME,
                       BR_ADDRESS,
                       BR_PHONE,
                       EMP_CODE
                   )
                   VALUES (
                       1,
                       'Decoffee',
                       'Meaw',
                       '@BUU',
                       '000-000-0000',
                       NULL
                   );


-- Table: CHECK_STOCK
DROP TABLE IF EXISTS CHECK_STOCK;

CREATE TABLE IF NOT EXISTS CHECK_STOCK (
    CHECK_CODE     INTEGER  PRIMARY KEY ASC AUTOINCREMENT
                            UNIQUE,
    EMP_CODE       INTEGER  NOT NULL
                            REFERENCES EMPLOYEE (EMP_CODE) ON DELETE RESTRICT
                                                           ON UPDATE CASCADE,
    CHECK_DATETIME DATETIME NOT NULL,
    FOREIGN KEY (
        EMP_CODE
    )
    REFERENCES EMPLOYEE (EMP_CODE) 
);


-- Table: CUSTOMER
DROP TABLE IF EXISTS CUSTOMER;

CREATE TABLE IF NOT EXISTS CUSTOMER (
    CUST_CODE    INTEGER        PRIMARY KEY AUTOINCREMENT
                                UNIQUE,
    CUST_FNAME   VARCHAR (20)   NOT NULL,
    CUST_LNAME   VARCHAR (20)   NOT NULL,
    CUST_EMAIL   VARCHAR (30)   NOT NULL,
    CUST_TEL     CHAR (10)      CHECK (CUST_TEL LIKE '___-___-____') 
                                NOT NULL,
    CUST_ADD     VARCHAR (100)  NOT NULL,
    CUST_GEN     VARCHAR (6)    NOT NULL,
    CUST_INDATE  DATE           NOT NULL,
    CUST_P_VALUE NUMERIC (5, 2) CHECK (CUST_P_VALUE >= 0.0 AND 
                                       CUST_P_VALUE <= 999.99) 
                                NOT NULL,
    CUST_P_RATE  NUMERIC (5, 2) CHECK (CUST_P_RATE >= 0.0 AND 
                                       CUST_P_RATE <= 999.99) 
                                NOT NULL
);


-- Table: DETAIL_PURCHASE_ORDER
DROP TABLE IF EXISTS DETAIL_PURCHASE_ORDER;

CREATE TABLE IF NOT EXISTS DETAIL_PURCHASE_ORDER (
    POD_CODE           INTEGER        PRIMARY KEY ASC AUTOINCREMENT
                                      UNIQUE,
    M_CODE             INTEGER        NOT NULL
                                      REFERENCES MATERIAL (M_CODE) ON DELETE RESTRICT
                                                                   ON UPDATE CASCADE,
    POD_AMOUNT         NUMERIC (6, 2) CHECK (POD_AMOUNT >= 0.0 AND 
                                             POD_AMOUNT <= 9999.0) 
                                      NOT NULL,
    POD_PRICE_PER_UNIT NUMERIC (6, 2) CHECK (POD_PRICE_PER_UNIT >= 0.0 AND 
                                             POD_PRICE_PER_UNIT <= 9999.0) 
                                      NOT NULL,
    POD_NOTE           VARCHAR (100)  NOT NULL,
    POD_MFD            DATE           NOT NULL,
    POD_EXD            DATE           NOT NULL,
    PO_CODE            INTEGER        NOT NULL
                                      REFERENCES PURCHASE_ORDER (PO_CODE) ON DELETE RESTRICT
                                                                          ON UPDATE CASCADE,
    FOREIGN KEY (
        M_CODE
    )
    REFERENCES MATERIAL (M_CODE),
    FOREIGN KEY (
        PO_CODE
    )
    REFERENCES PURCHASE_ORDER (PO_CODE) 
);


-- Table: EMPLOYEE
DROP TABLE IF EXISTS EMPLOYEE;

CREATE TABLE IF NOT EXISTS EMPLOYEE (
    EMP_CODE    INTEGER        PRIMARY KEY ASC AUTOINCREMENT
                               UNIQUE,
    EMP_FNAME   VARCHAR (20)   NOT NULL,
    EMP_LNAME   VARCHAR (20)   NOT NULL,
    EMP_EMAIL   VARCHAR (30)   NOT NULL,
    EMP_TEL     CHAR (10)      CHECK (EMP_TEL LIKE '___-___-____') 
                               NOT NULL,
    EMP_INDATE  DATE           NOT NULL,
    EMP_ADDRESS VARCHAR (100)  NOT NULL,
    BR_CODE     INTEGER        NOT NULL
                               REFERENCES BRANCH (BR_CODE) ON DELETE RESTRICT
                                                           ON UPDATE CASCADE,
    EMP_RANK    VARCHAR (10)   NOT NULL,
    EMP_STATUS  VARCHAR (10)   NOT NULL,
    EMP_SALARY  NUMERIC (8, 2) CHECK (EMP_SALARY >= 0.0 AND 
                                      EMP_SALARY <= 999999.99) 
                               NOT NULL,
    EMP_PASS    TEXT (20)      NOT NULL
                               UNIQUE,
    FOREIGN KEY (
        BR_CODE
    )
    REFERENCES BRANCH (BR_CODE) 
);


-- Table: ENTRY
DROP TABLE IF EXISTS ENTRY;

CREATE TABLE IF NOT EXISTS ENTRY (
    ENT_CODE     INTEGER      PRIMARY KEY ASC AUTOINCREMENT
                              UNIQUE,
    EMP_CODE     INTEGER      NOT NULL
                              REFERENCES EMPLOYEE (EMP_CODE) ON DELETE RESTRICT
                                                             ON UPDATE CASCADE,
    ENT_DATE     DATETIME     NOT NULL,
    ENT_TIME_IN  DATETIME     NOT NULL,
    ENT_TIME_OUT DATETIME     NOT NULL,
    ENT_STATUS   VARCHAR (10) NOT NULL,
    FOREIGN KEY (
        EMP_CODE
    )
    REFERENCES EMPLOYEE (EMP_CODE) 
);


-- Table: MATERIAL
DROP TABLE IF EXISTS MATERIAL;

CREATE TABLE IF NOT EXISTS MATERIAL (
    M_CODE           INTEGER        PRIMARY KEY ASC AUTOINCREMENT
                                    UNIQUE
                                    NOT NULL,
    M_NAME           VARCHAR (20)   NOT NULL,
    M_QOH            NUMERIC (6, 2) CHECK (M_QOH >= 0.0 AND 
                                           M_QOH <= 9999.0) 
                                    NOT NULL,
    M_PRICE_PER_UNIT NUMERIC (6, 2) CHECK (M_PRICE_PER_UNIT >= 0.0 AND 
                                           M_PRICE_PER_UNIT <= 9999.0) 
                                    NOT NULL,
    M_UNIT           VARCHAR (20)   NOT NULL,
    M_MIN            NUMERIC (6, 2) CHECK (M_MIN >= 0.0 AND 
                                           M_MIN <= 9999.0) 
                                    NOT NULL
);

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         1,
                         'Coco',
                         10,
                         20,
                         'Kilogram',
                         20
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         2,
                         'Milk',
                         20,
                         20,
                         'Liter',
                         100.5
                     );

INSERT INTO MATERIAL (
                         M_CODE,
                         M_NAME,
                         M_QOH,
                         M_PRICE_PER_UNIT,
                         M_UNIT,
                         M_MIN
                     )
                     VALUES (
                         3,
                         'Sugar',
                         30,
                         20,
                         'Kilogram',
                         20
                     );


-- Table: PAYROLL
DROP TABLE IF EXISTS PAYROLL;

CREATE TABLE IF NOT EXISTS PAYROLL (
    PRLSLIP_CODE INTEGER  PRIMARY KEY ASC AUTOINCREMENT
                          UNIQUE,
    EMP_CODE     INTEGER  NOT NULL
                          REFERENCES EMPLOYEE (EMP_CODE) ON DELETE RESTRICT
                                                         ON UPDATE CASCADE,
    PRL_STATUS   CHAR (8) NOT NULL,
    PRL_DATE     DATE     NOT NULL,
    ENT_CODE     CHAR (8) NOT NULL
                          REFERENCES ENTRY (ENT_CODE) ON DELETE RESTRICT
                                                      ON UPDATE CASCADE,
    FOREIGN KEY (
        EMP_CODE
    )
    REFERENCES EMPLOYEE (EMP_CODE),
    FOREIGN KEY (
        ENT_CODE
    )
    REFERENCES ENTRY (ENT_CODE) 
);


-- Table: PRODUCT
DROP TABLE IF EXISTS PRODUCT;

CREATE TABLE IF NOT EXISTS PRODUCT (
    PROD_CODE         INTEGER        PRIMARY KEY ASC AUTOINCREMENT
                                     UNIQUE,
    PROD_NAME         VARCHAR (20)   NOT NULL,
    PROD_PRICE        NUMERIC (5, 2) CHECK (PROD_PRICE >= 0.0 AND 
                                            PROD_PRICE <= 999.99) 
                                     NOT NULL,
    PROD_CATEGORY     VARCHAR (10)   NOT NULL,
    PROD_SUB_CATEGORY VARCHAR (10)   NOT NULL,
    PROD_AMOUNT       INTEGER        CHECK (PROD_AMOUNT >= 0 AND 
                                            PROD_AMOUNT <= 999) 
                                     NOT NULL,
    PROD_SIZE         VARCHAR (6)    NOT NULL
);


-- Table: PROMOTION
DROP TABLE IF EXISTS PROMOTION;

CREATE TABLE IF NOT EXISTS PROMOTION (
    PRO_CODE      INTEGER       PRIMARY KEY ASC AUTOINCREMENT
                                UNIQUE,
    PRO_NAME      VARCHAR (30)  NOT NULL,
    PRO_STARTDATE DATE          NOT NULL,
    PRO_ENDDATE   DATE          NOT NULL,
    PRO_DETAIL    VARCHAR (100) NOT NULL,
    PRO_STATUS    VARCHAR (10)  NOT NULL
);

INSERT INTO PROMOTION (
                          PRO_CODE,
                          PRO_NAME,
                          PRO_STARTDATE,
                          PRO_ENDDATE,
                          PRO_DETAIL,
                          PRO_STATUS
                      )
                      VALUES (
                          1,
                          'AD',
                          '2023-01-02 00:02:30',
                          '2023-01-02 00:04:30',
                          'awdawd',
                          'on'
                      );

INSERT INTO PROMOTION (
                          PRO_CODE,
                          PRO_NAME,
                          PRO_STARTDATE,
                          PRO_ENDDATE,
                          PRO_DETAIL,
                          PRO_STATUS
                      )
                      VALUES (
                          2,
                          'sale',
                          '2023-10-01 12:19:07',
                          '2023-10-01 12:19:07',
                          'de',
                          'on'
                      );


-- Table: PROMOTION_DETAIL
DROP TABLE IF EXISTS PROMOTION_DETAIL;

CREATE TABLE IF NOT EXISTS PROMOTION_DETAIL (
    PRDE_CODE     INTEGER        PRIMARY KEY ASC AUTOINCREMENT
                                 UNIQUE,
    RC_CODE       INTEGER        NOT NULL
                                 REFERENCES RECEIPT (RC_CODE) ON DELETE RESTRICT
                                                              ON UPDATE CASCADE,
    PRO_CODE      INTEGER        NOT NULL
                                 REFERENCES PROMOTION (PRO_CODE) ON DELETE RESTRICT
                                                                 ON UPDATE CASCADE,
    PRDE_DISCOUNT NUMERIC (6, 2) CHECK (PRDE_DISCOUNT >= 0.0 AND 
                                        PRDE_DISCOUNT <= 9999.99) 
                                 NOT NULL,
    FOREIGN KEY (
        PRO_CODE
    )
    REFERENCES PROMOTION (PRO_CODE) 
);


-- Table: PURCHASE_ORDER
DROP TABLE IF EXISTS PURCHASE_ORDER;

CREATE TABLE IF NOT EXISTS PURCHASE_ORDER (
    PO_CODE          INTEGER        PRIMARY KEY ASC AUTOINCREMENT
                                    UNIQUE,
    EMP_CODE         INTEGER        NOT NULL
                                    REFERENCES EMPLOYEE (EMP_CODE) ON DELETE RESTRICT
                                                                   ON UPDATE CASCADE,
    V_CODE           INTEGER        NOT NULL
                                    REFERENCES VENDOR (V_CODE) ON DELETE RESTRICT
                                                               ON UPDATE CASCADE,
    BR_CODE          INTEGER        NOT NULL
                                    REFERENCES BRANCH (BR_CODE) ON DELETE RESTRICT
                                                                ON UPDATE CASCADE,
    PO_TOTAL         NUMERIC (6, 2) CHECK (PO_TOTAL >= 0.0 AND 
                                           PO_TOTAL <= 9999.0) 
                                    NOT NULL,
    PO_TOTAL_PRICE   NUMERIC (7, 2) CHECK (PO_TOTAL_PRICE >= 0.0 AND 
                                           PO_TOTAL_PRICE <= 99999.0) 
                                    NOT NULL,
    PO_PAYDATE       DATE           NOT NULL,
    PO_RECEIVED_DATE DATE           NOT NULL,
    PO_DATE          DATE           NOT NULL,
    FOREIGN KEY (
        EMP_CODE
    )
    REFERENCES EMPLOYEE (EMP_CODE),
    FOREIGN KEY (
        V_CODE
    )
    REFERENCES VENDOR (V_CODE),
    FOREIGN KEY (
        BR_CODE
    )
    REFERENCES BRANCH (BR_CODE) 
);


-- Table: RECEIPT
DROP TABLE IF EXISTS RECEIPT;

CREATE TABLE IF NOT EXISTS RECEIPT (
    RC_CODE      INTEGER        PRIMARY KEY ASC AUTOINCREMENT
                                UNIQUE,
    EMP_CODE     INTEGER        NOT NULL
                                REFERENCES EMPLOYEE (EMP_CODE) ON DELETE RESTRICT
                                                               ON UPDATE CASCADE,
    CUST_ID      INTEGER        REFERENCES CUSTOMER (CUST_CODE) ON DELETE RESTRICT
                                                                ON UPDATE CASCADE,
    BR_CODE      INTEGER        NOT NULL
                                REFERENCES BRANCH (BR_CODE) ON DELETE RESTRICT
                                                            ON UPDATE CASCADE,
    RE_TOTAL     NUMERIC (5, 2) CHECK (RE_TOTAL >= 0.0 AND 
                                       RE_TOTAL <= 999.99) 
                                NOT NULL,
    RE_DATE      DATE           NOT NULL,
    RE_PAYMENT   NUMERIC (5, 2) CHECK (RE_PAYMENT >= 0.0 AND 
                                       RE_PAYMENT <= 999.99) 
                                NOT NULL,
    RE_CHANGE    VARCHAR (20)   NOT NULL,
    RE_NET_TOTAL NUMERIC (5, 2) CHECK (RE_NET_TOTAL >= 0.0 AND 
                                       RE_NET_TOTAL <= 999.99) 
                                NOT NULL,
    RE_DISCOUNT  NUMERIC (6, 2) CHECK (RE_DISCOUNT >= 0.0 AND 
                                       RE_DISCOUNT <= 9999.99) 
                                NOT NULL,
    FOREIGN KEY (
        EMP_CODE
    )
    REFERENCES EMPLOYEE (EMP_CODE),
    FOREIGN KEY (
        CUST_ID
    )
    REFERENCES CUSTOMER (CUST_CODE),
    FOREIGN KEY (
        BR_CODE
    )
    REFERENCES BRANCH (BR_CODE) 
);


-- Table: RECEIPT_DETAIL
DROP TABLE IF EXISTS RECEIPT_DETAIL;

CREATE TABLE IF NOT EXISTS RECEIPT_DETAIL (
    RCD_CODE       INTEGER        PRIMARY KEY ASC AUTOINCREMENT
                                  UNIQUE,
    RC_CODE        INTEGER        NOT NULL
                                  REFERENCES RECEIPT (RC_CODE) ON DELETE RESTRICT
                                                               ON UPDATE CASCADE,
    PROD_CODE      INTEGER        NOT NULL
                                  REFERENCES PRODUCT (PROD_CODE) ON DELETE RESTRICT
                                                                 ON UPDATE CASCADE,
    RCD_AMOUNT     NUMERIC (6, 2) CHECK (RCD_AMOUNT >= 0.0 AND 
                                         RCD_AMOUNT <= 9999.99) 
                                  NOT NULL,
    RCD_UNIT_PRICE NUMERIC (6, 2) CHECK (RCD_UNIT_PRICE >= 0.0 AND 
                                         RCD_UNIT_PRICE <= 9999.99) 
                                  NOT NULL,
    RCD_TOTAL      NUMERIC (6, 2) CHECK (RCD_TOTAL >= 0.0 AND 
                                         RCD_TOTAL <= 9999.99) 
                                  NOT NULL,
    RCD_DISCOUNT   NUMERIC (6, 2) CHECK (RCD_DISCOUNT >= 0.0 AND 
                                         RCD_DISCOUNT <= 9999.99) 
                                  NOT NULL,
    PRDE_CODE      INTEGER        REFERENCES PROMOTION_DETAIL (PRDE_CODE) ON DELETE RESTRICT
                                                                          ON UPDATE CASCADE,
    FOREIGN KEY (
        PROD_CODE
    )
    REFERENCES PRODUCT (PROD_CODE),
    FOREIGN KEY (
        PRDE_CODE
    )
    REFERENCES PROMOTION_DETAIL (PRDE_CODE) 
);


-- Table: STOCK_CHECK_DETAIL
DROP TABLE IF EXISTS STOCK_CHECK_DETAIL;

CREATE TABLE IF NOT EXISTS STOCK_CHECK_DETAIL (
    SCD_CODE             INTEGER        PRIMARY KEY ASC AUTOINCREMENT
                                        UNIQUE,
    M_CODE               INTEGER        NOT NULL
                                        REFERENCES MATERIAL (M_CODE) ON DELETE RESTRICT
                                                                     ON UPDATE CASCADE,
    CHECK_CODE           INTEGER        NOT NULL
                                        REFERENCES CHECK_STOCK (CHECK_CODE) ON DELETE RESTRICT
                                                                            ON UPDATE CASCADE,
    SCD_REMAINING_AMOUNT NUMERIC (7, 2) CHECK (SCD_REMAINING_AMOUNT >= 0.0 AND 
                                               SCD_REMAINING_AMOUNT <= 99999.0) 
                                        NOT NULL,
    SCD_EXPIRED_AMOUNT   NUMERIC (7, 2) CHECK (SCD_EXPIRED_AMOUNT >= 0.0 AND 
                                               SCD_EXPIRED_AMOUNT <= 99999.0) 
                                        NOT NULL,
    SCD_TOTAL_LOST       NUMERIC (7, 2) CHECK (SCD_TOTAL_LOST >= 0.0 AND 
                                               SCD_TOTAL_LOST <= 99999.0) 
                                        NOT NULL,
    FOREIGN KEY (
        M_CODE
    )
    REFERENCES MATERIAL (M_CODE),
    FOREIGN KEY (
        CHECK_CODE
    )
    REFERENCES CHECK_STOCK (CHECK_CODE) 
);


-- Table: VENDOR
DROP TABLE IF EXISTS VENDOR;

CREATE TABLE IF NOT EXISTS VENDOR (
    V_CODE     INTEGER       PRIMARY KEY ASC AUTOINCREMENT
                             UNIQUE,
    V_COUNTRY  VARCHAR (35)  NOT NULL,
    V_ADDRESS  VARCHAR (100) NOT NULL,
    V_PHONE    CHAR (12)     CHECK (V_PHONE LIKE '___-___-____') 
                             NOT NULL,
    V_NAME     VARCHAR (35)  NOT NULL,
    V_CONTACT  VARCHAR (30)  NOT NULL,
    V_CATEGORY VARCHAR (25)  NOT NULL
);


COMMIT TRANSACTION;
PRAGMA foreign_keys = on;
